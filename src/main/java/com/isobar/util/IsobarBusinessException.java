package com.isobar.util;

import java.util.function.Supplier;

import org.springframework.http.HttpStatus;

public class IsobarBusinessException extends RuntimeException implements Supplier {

	private static final long serialVersionUID = -6125298283326234461L;

	private HttpStatus httpStatus;

	public IsobarBusinessException () {
		super();
	}

	public IsobarBusinessException (HttpStatus httpStatus, String message) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public IsobarBusinessException (HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public IsobarBusinessException (String message) {
		super(message);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	@Override
	public Object get() {
		return this;
	}


}
