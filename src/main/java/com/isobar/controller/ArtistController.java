package com.isobar.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isaobar.service.ArtistService;
import com.isobar.entity.Artist;
import com.isobar.util.IsobarBusinessException;

@Controller
@RequestMapping("/isobar")
public class ArtistController {

	@Autowired
	private ArtistService artistService;

	@SuppressWarnings("unchecked")
	@GetMapping("/list-all")
    @ResponseBody
	public List<Artist> artistListAll() {
		Optional<List<Artist>> artists = artistService.listAll();
		return artists.map(art -> {
			return art;
		}).orElseThrow(new IsobarBusinessException(HttpStatus.NOT_FOUND));
	}

}
