package com.isobar;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableRabbit
@EnableScheduling
@EnableRedisRepositories
@ComponentScan("com.isobar.*")
@SpringBootApplication
public class IsobarApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsobarApplication.class, args);
	}

}
