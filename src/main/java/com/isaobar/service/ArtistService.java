package com.isaobar.service;

import java.util.List;
import java.util.Optional;

import com.isobar.entity.Artist;

public interface ArtistService {

	Optional<List<Artist>> listAll();

}