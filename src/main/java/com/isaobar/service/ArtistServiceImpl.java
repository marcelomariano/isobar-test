package com.isaobar.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.isobar.entity.Artist;

@Service
public class ArtistServiceImpl implements ArtistService {

	@Autowired
	private Logger logger;

	@Autowired
	private RestTemplate restTemplate;

	final String URI = "https://iws-recruiting-bands.herokuapp.com/api/full";

	@Override
	@Cacheable
	public Optional<List<Artist>> listAll() {
		logger.info("List all artists end-point");

		ResponseEntity<List<Artist>> response = restTemplate.exchange(URI,
				HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Artist>>() {
				});
		List<Artist> artistis = response.getBody();

		return Optional.ofNullable(artistis);
	}
}
