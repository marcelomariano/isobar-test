package com.isobar;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.isaobar.service.ArtistService;
import com.isobar.controller.ArtistController;
import com.isobar.entity.Artist;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IsobarApplicationTests {
	
	@Autowired
	@MockBean
	private ArtistService artistService;
	
	Optional<List<Artist>> artists;

	@Before
	public void setUp() {
		artists = artistService.listAll();
	}
	
	@Test
	public void contextLoads() {
		Mockito.when(artists.isPresent()).thenReturn(true);
	}

}
